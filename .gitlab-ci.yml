image: docker:stable
services:
  - docker:dind

variables:
  # Instruct Testcontainers to use the daemon of DinD.
  DOCKER_HOST: "tcp://docker:2375"
  # Improve performance with overlayfs.
  DOCKER_DRIVER: overlay2
  SPRING_PROFILES_ACTIVE: gitlab-ci
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  # -s .m2/settings.xml #if exists artifactory
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

stages:
  - test
  - coverage
  - version
  - build  
  - package
  # - deploy

testversion:
  image: maven:3-jdk-12-alpine
  stage: coverage
  before_script:
    - sed -i 's,@@VERSION@@,'$CI_COMMIT_REF_NAME',g' ./zoneslots/pom.xml
    - cd zoneslots && mvn install -B
  script: 
    - cat target/site/jacoco/index.html
  only:
    - master
  coverage: '/Total.*?([0-9]{1,3})%/'
      
tag-version:
  image: alaksmana/semver:latest
  before_script:
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - eval $(ssh-agent -s)
    - echo "$CI_DEPLOY_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - echo "$CI_DEPLOY_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo "$CI_DEPLOY_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - ls ~/.ssh
  stage: version
  script:
    # Get version, exit early if tag already exists
    - export SEMVER_LAST_TAG=$(git describe --abbrev=0 --tags 2>/dev/null)
    - export SEMVER_RELEASE_LEVEL=$(git log --oneline -1 --pretty=%B | cat | tr -d '\n' | cut -d "[" -f2 | cut -d "]" -f1)
    - if [ -z $SEMVER_LAST_TAG ]; then >&2 echo "No tags defined"; SEMVER_LAST_TAG="0.0.0"; fi
    - if [ -n $SEMVER_RELEASE_LEVEL ]; then >&2 echo "Release level ";echo $SEMVER_RELEASE_LEVEL;else >&2 echo "No release level defined, [patch] will be used."; export SEMVER_RELEASE_LEVEL=patch; fi
    - export SEMVER_VERSION=$(/opt/semver-tool.sh bump $SEMVER_RELEASE_LEVEL $SEMVER_LAST_TAG)
    - git tag
    - echo $SEMVER_VERSION
    - export SEMVER_VERSION_TAG_EXISTS=$(git tag | grep $SEMVER_VERSION | wc -l)
    - test $SEMVER_VERSION_TAG_EXISTS -eq 1 && exit 0 # exit early if the version tag already exists
    # Push new tag
    - export SEMVER_REMOTE_EXIST=$(git remote | grep gitlab)
    - if [ -z $SEMVER_REMOTE_EXIST]; then >&2 echo "No remote gitlab defined"; git remote add gitlab "$SEMVER_REPO_URL";else >&2 echo "Remote gitlab defined, removing it"; git remote remove gitlab; git remote add gitlab "$SEMVER_REPO_URL"; fi
    - git config --global user.name "$REG_USER"
    - git config --global user.email "$REG_USER_EMAIL"
    # Linknya harus diganti
    - git remote set-url origin ssh://git@gitlab.com/alaksmana/parkirin.git
    - git remote -v
    # - git fetch gitlab
    - git tag $SEMVER_VERSION
    - git push origin $SEMVER_VERSION
  only:
    - master

test:
  image: docker:latest
  stage: test
  variables:
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  before_script:
    - sed -i 's,@@VERSION@@,'$CI_COMMIT_REF_NAME',g' ./zoneslots/pom.xml
  script: 
    - cd zoneslots && docker run -t --rm -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd)":"$(pwd)" -w "$(pwd)" -u 0:0 openjdk:12 ./mvnw verify
  only:
    - tags

coverage:
  image: maven:3-jdk-12-alpine
  stage: coverage
  before_script:
    - sed -i 's,@@VERSION@@,'$CI_COMMIT_REF_NAME',g' ./zoneslots/pom.xml
    - cd zoneslots && mvn install -B
  script: 
    - cat target/site/jacoco/index.html
  only:
    - tags
  coverage: '/Total.*?([0-9]{1,3})%/'
  artifacts:
    paths:
      - zoneslots/target/site/jacoco/
  
sast:
  image: maven:3-jdk-12-alpine
  stage: coverage
  before_script:
    - sed -i 's,@@VERSION@@,'$CI_COMMIT_REF_NAME',g' ./zoneslots/pom.xml
    - cd zoneslots && mvn compile && mvn spotbugs:spotbugs
  script: 
    - cat target/spotbugsXml.xml
  only:
    - tags
  artifacts:
    paths:
      - zoneslots/target/spotbugsXml.xml

jarbuild:
  image: maven:3-jdk-12-alpine
  stage: build
  before_script:
    - sed -i 's,@@VERSION@@,'$CI_COMMIT_REF_NAME',g' ./zoneslots/pom.xml
  script: 
    - cd zoneslots && ./mvnw package -Dmaven.test.skip=true $MAVEN_CLI_OPTS
  artifacts:
    paths:
      - ./zoneslots/target/*.jar
  only:
    - tags
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

dockerize:
  # image: docker:19.03.0
  stage: package
  before_script:
    - echo "$DOCKERHUB_PASSWORD" | docker login --username "$DOCKERHUB_USER" --password-stdin docker.io
  script:
    - chmod +x ./zoneslots/entrypoint.sh
    - docker build -t $DOCKERHUB_USER/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME} ./zoneslots/.
    - docker push $DOCKERHUB_USER/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
    - docker tag $DOCKERHUB_USER/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME} $DOCKERHUB_USER/${CI_PROJECT_NAME}:latest
    - docker push $DOCKERHUB_USER/${CI_PROJECT_NAME}:latest
  only:
    - tags
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

pages:
  stage: package
  dependencies:
    - coverage
    - sast
  script:
    - mv zoneslots/target/site/jacoco/ public/
    - mv zoneslots/target/spotbugsXml.xml public/spotbugsXml.xml
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - tags

# deploy:
#   variables:
#     DOCKER_HOST: ""
#   image: alaksmana/dind:latest
#   stage: deploy
#   # before_script:
#   #   - echo "$DOCKERHUB_PASSWORD" | docker login --username "$DOCKERHUB_USER" --password-stdin docker.io
#   script:
#     - docker pull $DOCKERHUB_USER/${CI_PROJECT_NAME}:latest
#     # - docker run --name demo -d --rm -p ${SERVER_PORT}:8080 ${CI_REGISTRY}/${CI_PROJECT_NAME}:latest
#     - cd docker-compose && docker-compose up -d
#   only:
#     - tags
#   retry:
#     max: 2
#     when:
#       - runner_system_failure
#       - stuck_or_timeout_failure
#   tags:
#     - alaksmana
#   when: manual
      
    