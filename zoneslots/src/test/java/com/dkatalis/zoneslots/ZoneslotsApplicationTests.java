package com.dkatalis.zoneslots;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import com.dkatalis.zoneslots.service.HasuraService;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.testcontainers.containers.wait.Wait;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ContextConfiguration(initializers = { ZoneslotsApplicationTests.Initializer.class })
public class ZoneslotsApplicationTests {

	@Value("${app.debugenabled}")
	private Boolean debugenabled;
	private static String dbname = "postgres"; // must match with test application properties
	private static String dbpassword = "my_password";// must match with test application properties
	private static Integer dbport = 5432;
	private static final String HASURA_GRAPHQL_DATABASE_URL = "postgres://postgres:my_password@postgresql-master:5432/postgres";
	private static final String HASURA_GRAPHQL_ENABLE_CONSOLE = "true";
	private static Integer hasuraport = 8080;

	// @Autowired
	// private HasuraService hasuraService;

	// @ClassRule
	public static Network network = Network.newNetwork();

	@ClassRule
	public static GenericContainer postgres = new GenericContainer("bitnami/postgresql:11")
			.withNetworkAliases("postgresql-master").withNetwork(network).withExposedPorts(dbport)
			.withFileSystemBind("src/main/resources/parkirin.sql", "/docker-entrypoint-initdb.d/parkirin.sql", BindMode.READ_WRITE)
			.withEnv("POSTGRESQL_DATABASE", dbname).withEnv("POSTGRESQL_USERNAME", dbname)
			.withEnv("POSTGRESQL_PASSWORD", dbpassword)
			.waitingFor(Wait.forLogMessage(".*database system is ready to accept connections.*", 1));

	@ClassRule
	public static GenericContainer graphqlengine = new GenericContainer("hasura/graphql-engine:v1.0.0-rc.1")
			// .withCommand("/bin/sh", "-c", "while true ; do printf 'HTTP/1.1 200
			// OK\\n\\nyay' | nc -l -p 80 | sleep 10; done")
			.withNetworkAliases("graphql-engine").withNetwork(network).withExposedPorts(hasuraport)
			.withEnv("HASURA_GRAPHQL_DATABASE_URL", HASURA_GRAPHQL_DATABASE_URL)
			.withEnv("HASURA_GRAPHQL_ENABLE_CONSOLE", HASURA_GRAPHQL_ENABLE_CONSOLE)
			// .waitingFor(Wait.forLogMessage(".*starting API server.*", 1));
			.waitingFor(Wait.forHttp("/healthz")
			.forStatusCode(200));
			

	@Autowired
	private MockMvc mvc;
	
	@Before
	public void initiateHasura() throws Exception{
		this.mvc.perform(get("/replacemetadata").header("Content-Type", "application/json"))
			.andExpect(status().isOk());
	}

	@Test
	public void contextLoads() throws Exception{
		Assertions.assertThat(postgres.isRunning());
		Assertions.assertThat(graphqlengine.isRunning());
		Assertions.assertThat(mvc).isNotNull();
		if(debugenabled){
			System.out.println("IP container graphqlengine = " + graphqlengine.getContainerIpAddress());
			System.out.println("PORT container graphqlengine = " + graphqlengine.getMappedPort(8080));
			System.out.println("Network alias container graphqlengine = " + graphqlengine.getNetworkAliases().toString());
		}		
	}


	static class Initializer
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"app.hasuraserviceurl=http://"+ graphqlengine.getContainerIpAddress() + ":" + graphqlengine.getMappedPort(8080) 
			).applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Test
	public void getInitialStatus() throws Exception{

		this.mvc.perform(get("/status").header("Content-Type", "application/json"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.parkirin_zoneslots[0].id", is(1)))
			.andExpect(jsonPath("$.data.result.data.parkirin_zoneslots[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.parkirin_zoneslots[1].id", is(2)))
			.andExpect(jsonPath("$.data.result.data.parkirin_zoneslots[1].parkirinid", is(1)))
			;
	}

	@Test
	public void createParkingLot()throws Exception{
		String body = "{\"name\":\"Zone C Menara Prima\",\"totalslots\":10}";

		this.mvc.perform(post("/createparkinglot").header("Content-Type", "application/json").content(body))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_zoneslots.returning[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_zoneslots.returning[0].id", is(3)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_zoneslots.returning[0].name", is("Zone C Menara Prima")))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_zoneslots.returning[0].totalslots", is(10)))
			;
		
		String missingname = "{\"totalslots\":10}";
		this.mvc.perform(post("/createparkinglot").header("Content-Type", "application/json").content(missingname))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing name property.")))
			;

		String missingtotalslots = "{\"name\":\"Zone C Menara Prima\"}";
		this.mvc.perform(post("/createparkinglot").header("Content-Type", "application/json").content(missingtotalslots))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing totalslots property.")))
			;

	}

	@Test
	public void parkAndLeaveWithColor() throws Exception{

		//PARK
		String body = "{\"zoneslotid\":2,\"slotid\":2,\"registrationnumber\":\"DK1234GG\",\"color\":\"GREEN\"}";

		this.mvc.perform(post("/park").header("Content-Type", "application/json").content(body))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(2)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].id", is(2)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("DK1234GG")))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].entrytime", notNullValue()))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].color", is("GREEN")))
			;

		//NEAREST
		String nearestbody = "{\"zoneslotid\":2}";
		this.mvc.perform(post("/nearest").header("Content-Type", "application/json").content(nearestbody))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result", is(1)))
			;

		//LEAVE
		this.mvc.perform(post("/leave").header("Content-Type", "application/json").content(body))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].zoneslotid", is(2)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].id", is(2)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].registrationnumber", is("DK1234GG")))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].entrytime", notNullValue()))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].color", is("GREEN")))
			;

		//NEGATIVE PARK
		String missingzoneslot = "{\"slotid\":2,\"registrationnumber\":\"DK1234GG\",\"color\":\"GREEN\"}";

		this.mvc.perform(post("/park").header("Content-Type", "application/json").content(missingzoneslot))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing zoneslotid property.")))
			;
		
		String missingslot = "{\"zoneslotid\":2,\"registrationnumber\":\"DK1234GG\",\"color\":\"GREEN\"}";
		this.mvc.perform(post("/park").header("Content-Type", "application/json").content(missingslot))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing slotid property.")))
			;

		String missingregistration = "{\"zoneslotid\":2,\"slotid\":2,\"color\":\"GREEN\"}";
		this.mvc.perform(post("/park").header("Content-Type", "application/json").content(missingregistration))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing registrationnumber property.")))
			;
		
		//NEGATIVE LEAVE
		this.mvc.perform(post("/leave").header("Content-Type", "application/json").content(missingzoneslot))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing zoneslotid property.")))
			;
		
		this.mvc.perform(post("/leave").header("Content-Type", "application/json").content(missingregistration))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing registrationnumber property.")))
			;
		
		//NEGATIVE NEAREST
		this.mvc.perform(post("/nearest").header("Content-Type", "application/json").content("{}"))
			.andExpect(status().isInternalServerError())
			.andExpect(jsonPath("$.data.error", is("Missing zoneslotid property.")))
			;
	}

	@Test
	public void parkAndLeaveWithNoColor() throws Exception{

		//PARK
		String body = "{\"zoneslotid\":2,\"slotid\":1,\"registrationnumber\":\"B1612UJI\"}";

		this.mvc.perform(post("/park").header("Content-Type", "application/json").content(body))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(2)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].id", is(1)))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("B1612UJI")))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].entrytime", notNullValue()))
			.andExpect(jsonPath("$.data.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
			;
		
		//NEAREST
		String nearestbody = "{\"zoneslotid\":2}";
		this.mvc.perform(post("/nearest").header("Content-Type", "application/json").content(nearestbody))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result", is(2)))
			;
		
		//LEAVE
		this.mvc.perform(post("/leave").header("Content-Type", "application/json").content(body))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].parkirinid", is(1)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].zoneslotid", is(2)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].id", is(1)))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].registrationnumber", is("B1612UJI")))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].entrytime", notNullValue()))
			.andExpect(jsonPath("$.data.result.data.delete_parkirin_slots.returning[0].color", is("unknown")))
			;
	}

	@Test
	public void inputFile() throws Exception{

		File f = new File("src/main/resources/parking_lot_file_inputs.txt");
		if(debugenabled){
			System.out.println(f.isFile()+"  "+f.getName()+f.exists());
		}
		FileInputStream fi1 = new FileInputStream(f);
		MockMultipartFile fstmp = new MockMultipartFile("file", f.getName(), "multipart/form-data",fi1);
		
		this.mvc.perform(multipart("/fileinput").file(fstmp))
				.andExpect(status().isOk())
				//output1
				.andExpect(jsonPath("$.data.output1.result.data.insert_parkirin_zoneslots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output1.result.data.insert_parkirin_zoneslots.returning[0].id", is(4)))
				.andExpect(jsonPath("$.data.output1.result.data.insert_parkirin_zoneslots.returning[0].name", is("6")))
				.andExpect(jsonPath("$.data.output1.result.data.insert_parkirin_zoneslots.returning[0].totalslots", is(6)))
				//output2
				.andExpect(jsonPath("$.data.output2.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output2.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output2.result.data.insert_parkirin_slots.returning[0].id", is(1)))
				.andExpect(jsonPath("$.data.output2.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-1234")))
				.andExpect(jsonPath("$.data.output2.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output3
				.andExpect(jsonPath("$.data.output3.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output3.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output3.result.data.insert_parkirin_slots.returning[0].id", is(2)))
				.andExpect(jsonPath("$.data.output3.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-9999")))
				.andExpect(jsonPath("$.data.output3.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output4
				.andExpect(jsonPath("$.data.output4.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output4.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output4.result.data.insert_parkirin_slots.returning[0].id", is(3)))
				.andExpect(jsonPath("$.data.output4.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-BB-0001")))
				.andExpect(jsonPath("$.data.output4.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output5
				.andExpect(jsonPath("$.data.output5.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output5.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output5.result.data.insert_parkirin_slots.returning[0].id", is(4)))
				.andExpect(jsonPath("$.data.output5.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-7777")))
				.andExpect(jsonPath("$.data.output5.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output6
				.andExpect(jsonPath("$.data.output6.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output6.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output6.result.data.insert_parkirin_slots.returning[0].id", is(5)))
				.andExpect(jsonPath("$.data.output6.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-2701")))
				.andExpect(jsonPath("$.data.output6.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output7
				.andExpect(jsonPath("$.data.output7.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output7.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output7.result.data.insert_parkirin_slots.returning[0].id", is(6)))
				.andExpect(jsonPath("$.data.output7.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-3141")))
				.andExpect(jsonPath("$.data.output7.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output8
				.andExpect(jsonPath("$.data.output8.result.data.delete_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output8.result.data.delete_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output8.result.data.delete_parkirin_slots.returning[0].id", is(6)))
				.andExpect(jsonPath("$.data.output8.result.data.delete_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-3141")))
				.andExpect(jsonPath("$.data.output8.result.data.delete_parkirin_slots.returning[0].color", is("unknown")))
				.andExpect(jsonPath("$.data.output8.totalcost", is(30)))
				//output9
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].id", is(1)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[0].id", is(1)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[0].registrationnumber", is("KA-01-HH-1234")))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[1].id", is(2)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[1].registrationnumber", is("KA-01-HH-9999")))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[2].id", is(3)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[2].registrationnumber", is("KA-01-BB-0001")))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[3].id", is(4)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[3].registrationnumber", is("KA-01-HH-7777")))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[4].id", is(5)))
				.andExpect(jsonPath("$.data.output9.result.data.parkirin_zoneslots[0].slots[4].registrationnumber", is("KA-01-HH-2701")))
				//output10
				.andExpect(jsonPath("$.data.output10.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output10.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output10.result.data.insert_parkirin_slots.returning[0].id", is(6)))
				.andExpect(jsonPath("$.data.output10.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-01-P-333")))
				.andExpect(jsonPath("$.data.output10.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output11
				.andExpect(jsonPath("$.data.output11.error", is("Zoneslots is full! No parking slot available!")))
				//output12
				.andExpect(jsonPath("$.data.output12.result.data.delete_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output12.result.data.delete_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output12.result.data.delete_parkirin_slots.returning[0].id", is(1)))
				.andExpect(jsonPath("$.data.output12.result.data.delete_parkirin_slots.returning[0].registrationnumber", is("KA-01-HH-1234")))
				.andExpect(jsonPath("$.data.output12.result.data.delete_parkirin_slots.returning[0].color", is("unknown")))
				.andExpect(jsonPath("$.data.output12.totalcost", is(30)))
				//output13
				.andExpect(jsonPath("$.data.output13.result.data.delete_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output13.result.data.delete_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output13.result.data.delete_parkirin_slots.returning[0].id", is(3)))
				.andExpect(jsonPath("$.data.output13.result.data.delete_parkirin_slots.returning[0].registrationnumber", is("KA-01-BB-0001")))
				.andExpect(jsonPath("$.data.output13.result.data.delete_parkirin_slots.returning[0].color", is("unknown")))
				.andExpect(jsonPath("$.data.output13.totalcost", is(50)))
				//output14
				.andExpect(jsonPath("$.data.output14.result.data.delete_parkirin_slots.returning", empty()))
				//output15
				.andExpect(jsonPath("$.data.output15.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output15.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output15.result.data.insert_parkirin_slots.returning[0].id", is(1)))
				.andExpect(jsonPath("$.data.output15.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("KA-09-HH-0987")))
				.andExpect(jsonPath("$.data.output15.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output16
				.andExpect(jsonPath("$.data.output16.result.data.insert_parkirin_slots.returning[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output16.result.data.insert_parkirin_slots.returning[0].zoneslotid", is(1)))
				.andExpect(jsonPath("$.data.output16.result.data.insert_parkirin_slots.returning[0].id", is(3)))
				.andExpect(jsonPath("$.data.output16.result.data.insert_parkirin_slots.returning[0].registrationnumber", is("CA-09-IO-1111")))
				.andExpect(jsonPath("$.data.output16.result.data.insert_parkirin_slots.returning[0].color", is("unknown")))
				//output17
				.andExpect(jsonPath("$.data.output17.error", is("Zoneslots is full! No parking slot available!")))
				//output18
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].parkirinid", is(1)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].id", is(1)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[0].id", is(1)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[0].registrationnumber", is("KA-09-HH-0987")))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[1].id", is(2)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[1].registrationnumber", is("KA-01-HH-9999")))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[2].id", is(3)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[2].registrationnumber", is("CA-09-IO-1111")))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[3].id", is(4)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[3].registrationnumber", is("KA-01-HH-7777")))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[4].id", is(5)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[4].registrationnumber", is("KA-01-HH-2701")))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[5].id", is(6)))
				.andExpect(jsonPath("$.data.output18.result.data.parkirin_zoneslots[0].slots[5].registrationnumber", is("KA-01-P-333")))
				;

		//NEGATIVE
		File n = new File("src/main/resources/negative.txt");
		if(debugenabled){
			System.out.println(n.isFile()+"  "+n.getName()+n.exists());
		}
		FileInputStream ni1 = new FileInputStream(n);
		MockMultipartFile nstmp = new MockMultipartFile("file", n.getName(), "multipart/form-data",ni1);

		this.mvc.perform(multipart("/fileinput").file(nstmp))
				.andExpect(status().isOk())
		;
	}
}
