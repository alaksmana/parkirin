SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE SCHEMA parkirin;
ALTER SCHEMA parkirin OWNER TO postgres;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE parkirin.history (
    id bigint NOT NULL,
    logtime timestamp with time zone NOT NULL,
    parkirinid integer NOT NULL,
    zoneslotsid integer NOT NULL,
    slotid integer NOT NULL,
    entrytime timestamp with time zone NOT NULL,
    exittime timestamp with time zone NOT NULL,
    color text
);


ALTER TABLE parkirin.history OWNER TO postgres;

CREATE SEQUENCE parkirin.history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE parkirin.history_id_seq OWNER TO postgres;

ALTER SEQUENCE parkirin.history_id_seq OWNED BY parkirin.history.id;

CREATE TABLE parkirin.parkirin (
    id integer NOT NULL,
    name text NOT NULL,
    address text,
    zoneslotsid integer NOT NULL
);

ALTER TABLE parkirin.parkirin OWNER TO postgres;

CREATE SEQUENCE parkirin.parkirin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parkirin.parkirin_id_seq OWNER TO postgres;

ALTER SEQUENCE parkirin.parkirin_id_seq OWNED BY parkirin.parkirin.id;

CREATE TABLE parkirin.slots (
    parkirinid integer NOT NULL,
    zoneslotid integer NOT NULL,
    id integer NOT NULL,
    registrationnumber text NOT NULL,
    entrytime timestamp with time zone NOT NULL,
    color text
);


ALTER TABLE parkirin.slots OWNER TO postgres;

CREATE TABLE parkirin.zoneslots (
    id integer NOT NULL,
    name text NOT NULL,
    totalslots integer NOT NULL,
    parkirinid integer NOT NULL
);


ALTER TABLE parkirin.zoneslots OWNER TO postgres;

CREATE SEQUENCE parkirin.zoneslots_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parkirin.zoneslots_id_seq OWNER TO postgres;

ALTER SEQUENCE parkirin.zoneslots_id_seq OWNED BY parkirin.zoneslots.id;

ALTER TABLE ONLY parkirin.history ALTER COLUMN id SET DEFAULT nextval('parkirin.history_id_seq'::regclass);

ALTER TABLE ONLY parkirin.parkirin ALTER COLUMN id SET DEFAULT nextval('parkirin.parkirin_id_seq'::regclass);

ALTER TABLE ONLY parkirin.zoneslots ALTER COLUMN id SET DEFAULT nextval('parkirin.zoneslots_id_seq'::regclass);

INSERT INTO parkirin.parkirin (id, name, address, zoneslotsid)
VALUES(1, 'menara prima', '',1);

INSERT INTO parkirin.zoneslots (id, name, totalslots, parkirinid)
VALUES(1, 'Zona A Menara Prima',6,1);
INSERT INTO parkirin.zoneslots (id, name, totalslots, parkirinid)
VALUES(2, 'Zona B Menara Prima',10,1);

SELECT pg_catalog.setval('parkirin.history_id_seq', 1, false);

SELECT pg_catalog.setval('parkirin.parkirin_id_seq', 1, true);

SELECT pg_catalog.setval('parkirin.zoneslots_id_seq', 2, true);

ALTER TABLE ONLY parkirin.history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);

ALTER TABLE ONLY parkirin.parkirin
    ADD CONSTRAINT parkirin_id_key UNIQUE (id);

ALTER TABLE ONLY parkirin.parkirin
    ADD CONSTRAINT parkirin_pkey PRIMARY KEY (id, zoneslotsid);

ALTER TABLE ONLY parkirin.slots
    ADD CONSTRAINT slots_pkey PRIMARY KEY (parkirinid, zoneslotid, id);

ALTER TABLE ONLY parkirin.slots
    ADD CONSTRAINT slots_registrationnumber_key UNIQUE (registrationnumber);

ALTER TABLE ONLY parkirin.zoneslots
    ADD CONSTRAINT zoneslots_pkey PRIMARY KEY (id, parkirinid);

ALTER TABLE ONLY parkirin.slots
    ADD CONSTRAINT slots_parkirinid_fkey FOREIGN KEY (parkirinid, zoneslotid) REFERENCES parkirin.zoneslots(parkirinid, id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY parkirin.zoneslots
    ADD CONSTRAINT zoneslots_parkirinid_fkey FOREIGN KEY (parkirinid) REFERENCES parkirin.parkirin(id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE TABLE parkirin.price (
    parkirinid integer NOT NULL,
    zoneslotid integer NOT NULL,
    id integer NOT NULL,
    price numeric NOT NULL
);


ALTER TABLE parkirin.price OWNER TO postgres;

CREATE SEQUENCE parkirin.price_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE parkirin.price_id_seq OWNER TO postgres;

ALTER SEQUENCE parkirin.price_id_seq OWNED BY parkirin.price.id;

INSERT INTO parkirin.price (parkirinid, zoneslotid, id, price)
VALUES(1,1,1,5), 
(1,1,2,5),
(1,1,3,10),
(1,1,4,10),
(1,1,5,10),
(1,1,6,10),
(1,1,7,10),
(1,1,8,10),
(1,1,9,10),
(1,1,10,10),
(1,1,11,10),
(1,1,12,10),
(1,2,1,5), 
(1,2,2,5),
(1,2,3,10),
(1,2,4,10),
(1,2,5,10),
(1,2,6,10),
(1,2,7,10),
(1,2,8,10),
(1,2,9,10),
(1,2,10,10),
(1,2,11,10),
(1,2,12,10);
