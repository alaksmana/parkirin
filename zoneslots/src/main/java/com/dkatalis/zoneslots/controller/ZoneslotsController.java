package com.dkatalis.zoneslots.controller;

import com.dkatalis.zoneslots.payload.UploadFileResponse;
import com.dkatalis.zoneslots.service.HasuraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;

@RestController
public class ZoneslotsController {
    @Autowired
    private HasuraService hasuraService;

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    @PostMapping(path = "/createparkinglot", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> createparkinglot(HttpServletRequest req, @RequestBody String bodystring) {
        
        // try{
                Document body = Document.parse(bodystring);
                Document result = hasuraService.executeCreateparkinglot(body);

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + ".createParkingLot");
                    response.setData(result);
                if(result.containsKey("error")){
                    response.setMessage("Method execution fail!");
                    return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + ".createParkingLot");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

    @PostMapping(path = "/park", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executePark(HttpServletRequest req, @RequestBody String bodystring) {
        
        // try{
            Document body = Document.parse(bodystring);
                Document result = hasuraService.executePark(body);

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + ".executePark");
                    response.setData(result);
                if(result.containsKey("error")){
                    response.setMessage("Method execution fail!");
                    return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + "createParkingLot");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

    @PostMapping(path = "/leave", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executeLeave(HttpServletRequest req, @RequestBody String bodystring) {
        
        // try{
            Document body = Document.parse(bodystring);
                Document result = hasuraService.executeLeave(body);

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + ".executeLeave");
                    response.setData(result);
                if(result.containsKey("error")){
                    response.setMessage("Method execution fail!");
                    return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + ".executeLeave");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

    @PostMapping(path = "/nearest", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executeNearestSlot(HttpServletRequest req, @RequestBody String bodystring) {
        
        // try{
            Document body = Document.parse(bodystring);
                Document result = hasuraService.executeNearestSlot(body);

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + ".executeLeave");
                    response.setData(result);
                if(result.containsKey("error")){
                    response.setMessage("Method execution fail!");
                    return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + ".executeLeave");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

    @GetMapping(path = "/status", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executeStatus(HttpServletRequest req) {
        
        // try{
            if(debugenabled){
                System.out.println("controller status called");
            }

                Document result = hasuraService.executeStatus();

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + "executeStatus");
                    response.setData(result);
                // if(result.containsKey("error")){
                //     response.setMessage("Method execution fail!");
                //     return ResponseEntity
                //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
                //         .contentType(MediaType.APPLICATION_JSON)
                //         .body(response)
                //     ;
                // }
                // else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                // }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + "executeStatus");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

    @GetMapping(path = "/replacemetadata", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executeReplaceMetadata(HttpServletRequest req) {
        
        // try{
            if(debugenabled){
                System.out.println("controller executeReplaceMetadata called");
            }

                Document result = hasuraService.executeReplaceMetadata();

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + "executeReplaceMetadata");
                    response.setData(result);
                // if(result.containsKey("error")){
                //     response.setMessage("Method execution fail!");
                //     return ResponseEntity
                //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
                //         .contentType(MediaType.APPLICATION_JSON)
                //         .body(response)
                //     ;
                // }
                // else{
                    response.setMessage("Method execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                // }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during Method execution!");
        //         response.setService(this.getClass().getName() + "executeReplaceMetadata");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }

}