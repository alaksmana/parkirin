package com.dkatalis.zoneslots.controller;

import com.dkatalis.zoneslots.payload.UploadFileResponse;
import com.dkatalis.zoneslots.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;

@RestController
public class ImportController {
    @Autowired
    private ImportService importService;

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    UploadFileResponse uploadFileresponse = new UploadFileResponse();
    
    @PostMapping(path = "/fileinput", consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity<UploadFileResponse> executeFile(HttpServletRequest req, @RequestParam("file") MultipartFile file) {
        
        // try{
                
                Document result = importService.runFile(file);

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + "executeFile");
                    response.setData(result);
                // if(result.containsKey("error")){
                //     response.setMessage("File execution fail!");
                //     return ResponseEntity
                //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
                //         .contentType(MediaType.APPLICATION_JSON)
                //         .body(response)
                //     ;
                // }
                // else{
                    response.setMessage("File execution successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                // }
                
        // } catch (Exception e){
        //     UploadFileResponse response = new UploadFileResponse();
        //         response.setMessage("Exception during File upload!");
        //         response.setService(this.getClass().getName() + "executeFile");
        //         response.setData(e);
        //     return ResponseEntity
        //         .status(HttpStatus.INTERNAL_SERVER_ERROR)
        //         .contentType(MediaType.APPLICATION_JSON)
        //         .body(response)
        //     ;
        // }
    }
}