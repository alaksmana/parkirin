package com.dkatalis.zoneslots.service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImportService{

    @Autowired
    private HasuraService hasuraService;

    @Value("${app.zoneslotid}")
    private Integer zoneslotid;

    @Value("${app.upload-dir}")
    private String uploaddir;

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    private String username = "username";
    private static final String OUTPUT = "output";
    private static final String ERROR = "error";
    private static final String ERRORCODE = "errorcode";
    private static final String FILEIMPORT = "FILE IMPORT";

    public Document runFile(MultipartFile file){
        Document result = new Document();        

        File uploadedfile;
        try{
            uploadedfile = this.saveFile(file);
        }
        catch(Exception e){
            result.put(ERROR, "Fail to save File in the storage = " +e.toString());
            return result;
        }
        if(debugenabled){
            System.out.println("Saved ok, file path = "+ uploadedfile.getAbsolutePath());
        }

        LineIterator it = null;
        try {
            it = FileUtils.lineIterator(uploadedfile, "UTF-8");
            int lineno = 1;
            while (it.hasNext()) {
                String line = it.nextLine();
                StringTokenizer st = new StringTokenizer(line);
                int linetokencount = st.countTokens();
                while (st.hasMoreTokens()) {
                    String sCommand = st.nextToken();
                    if(this.debugenabled){System.out.println(sCommand);}
                    
                    if("create_parking_lot".equalsIgnoreCase(sCommand)){
                        if(linetokencount!=2){
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " [totalslots]! " + uploadedfile.getAbsolutePath());
                            break;
                        }
                        String stringtotalslots = st.nextToken();
                        Integer totalslots = 0;
                        try {
                            totalslots = Integer.parseInt(stringtotalslots);
                        } catch (Exception e) {
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " [totalslots]! " + uploadedfile.getAbsolutePath());
                            break;
                        }
                            
                        result.put("output"+lineno, this.createParkingLot(totalslots));
                    }
                    else if("park".equalsIgnoreCase(sCommand)){
                        if(linetokencount!=2){
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " [registrationnumber]! " + uploadedfile.getAbsolutePath());
                            break;
                        }
                        String registrationnumber = st.nextToken();
                        result.put("output"+lineno, this.park(registrationnumber));
                    }
                    else if("leave".equalsIgnoreCase(sCommand)){
                        if(linetokencount!=3){
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " [registrationnumber][hours] |" + uploadedfile.getAbsolutePath());
                            break;
                        }
                        String registrationnumber = st.nextToken();
                        String stringhours = st.nextToken();
                        Integer hours = 0;
                        try {
                            hours = Integer.parseInt(stringhours);
                        } catch (Exception e) {
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " [registrationnumber][hours] |" + uploadedfile.getAbsolutePath());
                            break;
                        }
                        
                        result.append("output"+lineno, this.leave(registrationnumber, hours));
                    }
                    else if("status".equalsIgnoreCase(sCommand)){
                        if(linetokencount!=1){
                            result.put("output"+lineno, "Line["+ lineno + "] Usage:" + sCommand + " |" + uploadedfile.getAbsolutePath());
                            break;
                        }
                        result.append("output"+lineno, this.status());
                    }
                    else{
                        result.put("output"+lineno, "Line["+ lineno + "] Unknown command:" + sCommand + " |" + uploadedfile.getAbsolutePath());
                    }
                }//end tokenizer
                lineno++;
            }
        }
        catch(IOException e){
            result.put(ERROR, "Exception while executing file " + uploadedfile.getAbsolutePath() + " = " + e.toString());
            return result;
        }
        finally {
            if(null!=it){LineIterator.closeQuietly(it);}
        }


        //delete file
        try{
            FileUtils.forceDelete(uploadedfile);
            System.out.println("Successfully delete uploaded file = "+ uploadedfile.getAbsolutePath());
        }
        catch(Exception e){
            result.put(ERROR, "Fail to delete temporary uploaded file " + uploadedfile.getAbsolutePath() + " = " + e.toString());
            return result;
        }

        return result;
    }

    public Document createParkingLot(Integer totalslots){
        Document parambody =  new Document();
        // {
        //     "name" : "Zone A Menara Prima",
        //     "totalslots" : 6
        // }
        parambody.put("name", totalslots.toString());
        parambody.put("totalslots", totalslots);

        return this.hasuraService.executeCreateparkinglot(parambody);
    }

    public Document park(String registrationnumber){
        Document parambody =  new Document();
        //sample body
        // {
        //     "zoneslotid" : 1,
        //     "slotid" : 4,
        //     "registrationnumber" : "DK1234GG"
        //     "color" : "GREEN" //optional
        // }
        
        parambody.put("zoneslotid", zoneslotid);

        //find the closes available slot
        Document nearestslotdoc = this.hasuraService.executeNearestSlot(parambody);
        if(!nearestslotdoc.containsKey("result")){
            Document error = new Document();
            error.put(ERROR, "Zoneslots is full! No parking slot available!");
            return error;
        }
        Integer nearestslot = nearestslotdoc.getInteger("result");

        parambody.put("slotid", nearestslot);
        
        parambody.put("registrationnumber", registrationnumber);
        // parambody.put("color", "GREEN");
        return this.hasuraService.executePark(parambody);
    }

    public Document leave(String registrationnumber, Integer hours){
        Document parambody =  new Document();
        //sample body
        // {
        //     "zoneslotid" : 1,
        //     "slotid" : 4,
        //     "registrationnumber" : "DK1234GG"
        //     "color" : "GREEN" //optional
        // }
        parambody.put("zoneslotid", zoneslotid);

        //find the closes available slot
        // parambody.put("slotid", Integer.valueOf(3));

        parambody.put("registrationnumber", registrationnumber);
        parambody.put("parkinghours", hours);
        // parambody.put("color", "GREEN");
        if(debugenabled){System.out.println("zoneslot params for leaving = " + parambody.toJson());}
        return this.hasuraService.executeLeave(parambody);
    }

    public Document status(){
        return this.hasuraService.executeStatus();
    }

    public File saveFile(MultipartFile filePart) throws IOException{
        Timestamp filestamp = new Timestamp(System.currentTimeMillis());
        final String currentfunction = "saveFile";
        final String BASE_DIRECTORY = uploaddir + File.separator + "parkirin";

        if(debugenabled){
            System.out.println( "save txt upload file");
        }
        
            String filename = filestamp.getTime()+".txt";
            //create directory
            new File(BASE_DIRECTORY).mkdirs();

            if(debugenabled){
                System.out.println("Filename = " + filename);
            }
            
            File filex = new File(BASE_DIRECTORY, FilenameUtils.getName(filename));

            // Path filepath = Paths.get(filex.getPath());

            // if(debugenabled){
            //     System.out.println("file path = " + filepath.toString());
            // }
            if(filex.getAbsolutePath().startsWith(BASE_DIRECTORY)){
                FileUtils.writeByteArrayToFile(filex, filePart.getBytes());
                return filex;
            }
            else{
                throw new IOException("File is not in the designated UPLOAD DIRECTORY!");
            }

    }

}