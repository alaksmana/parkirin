package com.dkatalis.zoneslots.service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.dkatalis.zoneslots.entity.GraphQLQuery;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Service
public class HasuraService{

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    // @Value("${app.configsourceschema}")
    // private String configsourceschema;
    // @Value("${app.configsourcetable}")
    // private String configsourcetable;
    @Value("${app.hasuraserviceurl}")
    private String hasuraserviceurl;

    @Value("${app.parkirinid}")
    private Integer parkirinid;

    private String username = "username";
    private static final String OUTPUT = "output";
    private static final String ERROR = "error";
    private static final String ERRORCODE = "errorcode";
    private static final String FILEIMPORT = "FILE IMPORT";
    
    public Document executeCreateparkinglot(Document body){

        if(debugenabled){
            System.out.println("executeCreateparkinglot called");
        }
        Document result = new Document();

        if(!body.containsKey("name")){
            result.append("error", "Missing name property.");
            return result;
        }
        if(!body.containsKey("totalslots")){
            result.append("error", "Missing totalslots property.");
            return result;
        }
        String name = body.getString("name");
        Integer totalslots = body.getInteger("totalslots");

        //minify
        String query = "mutation{insert_parkirin_zoneslots(objects:{name:\""+ name + "\",totalslots: "+ totalslots + ",parkirinid: " + parkirinid + "}){returning{id name totalslots parkirinid}}}";

        Document resultobj = executeGraphQLQuery(query);
        if(debugenabled){
            System.out.println("executeGraphQLQuery result = " + resultobj.toString());
        }
 
        if(null!=resultobj){result.append("result", resultobj);}
        else{
            result.append("result", new Document());
        }

        return result;
    }

    public Document executePark(Document body){

        if(debugenabled){
            System.out.println("executePark called");
        }
        Document result = new Document();

        if(!body.containsKey("zoneslotid")){
            result.append("error", "Missing zoneslotid property.");
            return result;
        }
        if(!body.containsKey("slotid")){
            result.append("error", "Missing slotid property.");
            return result;
        }
        if(!body.containsKey("registrationnumber")){
            result.append("error", "Missing registrationnumber property.");
            return result;
        }
        String color = "unknown";
        if(body.containsKey("color")){
            color = body.getString("color");
        }

        Integer zoneslotid = body.getInteger("zoneslotid");
        Integer slotid = body.getInteger("slotid");
        String registrationnumber = body.getString("registrationnumber");
        
        //minify
        String query = "mutation{insert_parkirin_slots(objects:{parkirinid:" + parkirinid + ",zoneslotid: "+ zoneslotid +", id: " + slotid + ",registrationnumber: \"" + registrationnumber + "\",entrytime: \"now()\",color: \"" + color + "\"}){returning{parkirinid zoneslotid id registrationnumber entrytime color}}}";

        Document resultobj = executeGraphQLQuery(query);
        if(debugenabled){
            System.out.println("executeGraphQLQuery result = " + resultobj.toString());
        }
 
        if(null!=resultobj){result.append("result", resultobj);}
        else{
            result.append("result", new Document());
        }

        return result;
    }

    public Document executeLeave(Document body){
        if(debugenabled){
            System.out.println("executeLeave called");
        }
        Document result = new Document();

        if(!body.containsKey("zoneslotid")){
            result.append("error", "Missing zoneslotid property.");
            return result;
        }

        boolean calculateentry = false;        
        if(!body.containsKey("parkinghours")){ 
            //is there user input on the hours? No, calculate the hours
            calculateentry = true;
        }

        if(!body.containsKey("registrationnumber")){
            result.append("error", "Missing registrationnumber property.");
            return result;
        }
        String color = "unknown";
        if(body.containsKey("color")){
            color = body.getString("color");
        }

        Integer zoneslotid = body.getInteger("zoneslotid");
        // Integer slotid = body.getInteger("slotid");
        String registrationnumber = body.getString("registrationnumber");

        //minify
        // String query = "mutation{delete_parkirin_slots(where:{parkirinid:{_eq:" + parkirinid + "},zoneslotid:{_eq:"+ zoneslotid +"},id:{_eq:" + slotid + "},registrationnumber: {_eq: \"" + registrationnumber + "\"},color:{_eq:\"" + color + "\"}}){returning{parkirinid zoneslotid id registrationnumber color entrytime}}}";
        String query = "mutation{delete_parkirin_slots(where:{parkirinid:{_eq:" + parkirinid + "},zoneslotid:{_eq:"+ zoneslotid +"},registrationnumber: {_eq: \"" + registrationnumber + "\"},color:{_eq:\"" + color + "\"}}){returning{parkirinid zoneslotid id registrationnumber color entrytime}}}";

        Document resultobj = executeGraphQLQuery(query);
        if(debugenabled){
            System.out.println("executeGraphQLQuery result = " + resultobj.toJson());
        }
        if(null!=resultobj){result.append("result", resultobj);}
        else{
            result.append("result", new Document());
        }

        Integer parkinghours = 0;
        LocalDateTime entrytime =  null;
        LocalDateTime exittime = null;
        if(calculateentry){
            //use returned entrytime and get pricing, calculate cost and return total  cost
            // "data": {
            //     "delete_parkirin_slots": {
            //         "returning": [
            //             {
            //                 "parkirinid": 1,
            //                 "zoneslotid": 1,
            //                 "id": 4,
            //                 "registrationnumber": "DK1234GG",
            //                 "color": "GREEN",
            //                 "entrytime": "2019-12-09T16:53:06.513344+00:00"
            //             }
            //         ]
            //     }
            // }
            if(null!=resultobj){
                Document data = (Document) resultobj.get("data");
                Document delete_parkirin_slots = (Document) data.get("delete_parkirin_slots");
                List<Document> returning = (List<Document>) delete_parkirin_slots.getList("returning", Document.class);
                Document deletedentry = returning.get(0);
                String entrytimestring  = deletedentry.getString("entrytime");
                
                Calendar entrytimecal = javax.xml.bind.DatatypeConverter.parseDateTime(entrytimestring);
                
                TimeZone tz = entrytimecal.getTimeZone();
                ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();

                entrytime = LocalDateTime.ofInstant(entrytimecal.toInstant(), zid);

                exittime = LocalDateTime.now();
                Duration duration = Duration.between(entrytime, exittime);
                parkinghours = (int) duration.toHours();
            }
        }
        else{
            //use  given body hours as calculation
            parkinghours = body.getInteger("parkinghours");
        }

        BigDecimal totalcost = BigDecimal.ZERO;
        
        if(parkinghours>0){totalcost = this.executeGetTotalCost( parkirinid,   zoneslotid,  parkinghours);}
        if(null==totalcost){totalcost = BigDecimal.ZERO;}
        result.append("totalcost", totalcost);

        return result;
    }

    private BigDecimal executeGetTotalCost(Integer parkirinid,  Integer zoneslotid, Integer parkinghours){
        //get totalcost
        String pricingquery = "{parkirin_price_aggregate(where:{parkirinid:{_eq:"+parkirinid+"},zoneslotid:{_eq:"+zoneslotid+"},id:{_lte:"+parkinghours+"}}){aggregate{sum{price}}}}";
        Document pricingresult = executeGraphQLQuery(pricingquery);
        if(debugenabled){
            System.out.println("executeGraphQLQuery PRICING result = " + pricingresult.toJson());
        }

        if(null==pricingresult){return BigDecimal.ZERO;}

        // {
        //     "data": {
        //       "parkirin_price_aggregate": {
        //         "aggregate": {
        //           "sum": {
        //             "price": 30
        //           }
        //         }
        //       }
        //     }
        //   }

        Document data = (Document) pricingresult.get("data");
        Document parkirin_price_aggregate = (Document) data.get("parkirin_price_aggregate");
        Document aggregate = (Document) parkirin_price_aggregate.get("aggregate");
        Document sum = (Document) aggregate.get("sum");
        Integer totalcost = sum.getInteger("price");
        if(null==totalcost){return BigDecimal.ZERO;}
        BigDecimal newtotalcost = new BigDecimal(totalcost);

        return newtotalcost;
    }

    public Document executeStatus(){
        if(debugenabled){
            System.out.println("executeStatus called");
        }
        Document result = new Document();
        //minify
        String query = "query{parkirin_zoneslots{id name parkirinid totalslots slots{parkirinid zoneslotid id registrationnumber entrytime color}}}";

        Document resultobj = executeGraphQLQuery(query);
        if(debugenabled){
            System.out.println("executeGraphQLQuery result = " + resultobj.toString());
        }
 
        if(null!=resultobj){result.append("result", resultobj);}
        else{
            result.append("result", new Document());
        }

        return result;
    }

    public Document executeNearestSlot(Document body){
        if(debugenabled){
            System.out.println("executeNearestSlot called");
        }
        Document result = new Document();

        if(!body.containsKey("zoneslotid")){
            result.append("error", "Missing zoneslotid property.");
            return result;
        }

        Integer zoneslotid = body.getInteger("zoneslotid");

        //minify
        String query = "{parkirin_zoneslots(where:{id:{_eq:"+ zoneslotid +"}}){id name totalslots slots(order_by:{id:asc}){id}}}";

        Document resultobj = executeGraphQLQuery(query);
        if(debugenabled){
            System.out.println("executeGraphQLQuery result doc = " + resultobj.toJson());
        }

        //get smallest slot not in the result, sample:
        // {
        //     "data": {
        //       "parkirin_zoneslots": [
        //         {
        //           "id": 1,
        //           "name": "zone 1 menara prima",
        //           "totalslots": 6,
        //           "slots": [
        //             {
        //               "id": 1
        //             },
        //             {
        //               "id": 2
        //             },
        //             {
        //               "id": 3
        //             }
        //           ]
        //         }
        //       ]
        //     }
        //   }
        
        //get just the nearest available id

        Document data = (Document) resultobj.get("data");
        if(debugenabled){
            System.out.println("data = " + data.toJson());
        }
        List<Document> parkirin_zoneslots = (List<Document>) data.getList("parkirin_zoneslots", Document.class);
        Integer totalslots = 0;
        ArrayList<Integer> idlist = new ArrayList<>();        
        if(parkirin_zoneslots.size()>0){
            Document zoneslots =  parkirin_zoneslots.get(0);
            totalslots = zoneslots.getInteger("totalslots");
            List<Document> slots = (List<Document>) zoneslots.getList("slots", Document.class);
            if(debugenabled){
                for(Document idobj : slots){
                    System.out.println("slots content = " + idobj.toJson());
                }
            }
            Integer currentid = 1;
            Integer nextid = 1;
            Iterator<Document> islots = slots.iterator();
            boolean once = true;
            while(islots.hasNext()){
                Document idobj = islots.next();
                
                currentid = idobj.getInteger("id");
                if(currentid>nextid){ //a slot
                    result.append("result", nextid);
                    return result;
                }
                else{ //nextid == currentid
                    if(islots.hasNext()){
                        nextid = currentid + 1;
                        if(nextid>totalslots){ //is it the end?
                            return result; //not available
                        }
                    }
                    else{ //no more entries
                        nextid = currentid + 1;
                        if( nextid<=totalslots){ //slot available
                            result.append("result", nextid);
                            return result;
                        }
                    }
                }
                once = false;
            }
            if(once){
                result.append("result", nextid);
                return result;
            }
            
        }
 
        return result;
    }

    public Document executeReplaceMetadata(){
        if(debugenabled){
            System.out.println("executeReplaceMetadata called");
        }
        // Document result = new Document();

        //minify
        String query = "{\"type\":\"replace_metadata\",\"args\":{\"functions\":[],\"remote_schemas\":[],\"query_collections\":[],\"allowlist\":[],\"version\":2,\"tables\":[{\"table\":{\"schema\":\"parkirin\",\"name\":\"history\"},\"is_enum\":false,\"configuration\":{\"custom_root_fields\":{\"select\":null,\"select_by_pk\":null,\"select_aggregate\":null,\"insert\":null,\"update\":null,\"delete\":null},\"custom_column_names\":{}},\"object_relationships\":[],\"array_relationships\":[],\"insert_permissions\":[],\"select_permissions\":[],\"update_permissions\":[],\"delete_permissions\":[],\"event_triggers\":[],\"computed_fields\":[]},{\"table\":{\"schema\":\"parkirin\",\"name\":\"parkirin\"},\"is_enum\":false,\"configuration\":{\"custom_root_fields\":{\"select\":null,\"select_by_pk\":null,\"select_aggregate\":null,\"insert\":null,\"update\":null,\"delete\":null},\"custom_column_names\":{}},\"object_relationships\":[],\"array_relationships\":[{\"using\":{\"foreign_key_constraint_on\":{\"column\":\"parkirinid\",\"table\":{\"schema\":\"parkirin\",\"name\":\"zoneslots\"}}},\"name\":\"zoneslots\",\"comment\":null}],\"insert_permissions\":[],\"select_permissions\":[],\"update_permissions\":[],\"delete_permissions\":[],\"event_triggers\":[],\"computed_fields\":[]},{\"table\":{\"schema\":\"parkirin\",\"name\":\"price\"},\"is_enum\":false,\"configuration\":{\"custom_root_fields\":{\"select\":null,\"select_by_pk\":null,\"select_aggregate\":null,\"insert\":null,\"update\":null,\"delete\":null},\"custom_column_names\":{}},\"object_relationships\":[],\"array_relationships\":[],\"insert_permissions\":[],\"select_permissions\":[],\"update_permissions\":[],\"delete_permissions\":[],\"event_triggers\":[],\"computed_fields\":[]},{\"table\":{\"schema\":\"parkirin\",\"name\":\"slots\"},\"is_enum\":false,\"configuration\":{\"custom_root_fields\":{\"select\":null,\"select_by_pk\":null,\"select_aggregate\":null,\"insert\":null,\"update\":null,\"delete\":null},\"custom_column_names\":{}},\"object_relationships\":[{\"using\":{\"manual_configuration\":{\"remote_table\":{\"schema\":\"parkirin\",\"name\":\"zoneslots\"},\"column_mapping\":{\"parkirinid\":\"parkirinid\",\"zoneslotid\":\"id\"}}},\"name\":\"zoneslot\",\"comment\":null}],\"array_relationships\":[],\"insert_permissions\":[],\"select_permissions\":[],\"update_permissions\":[],\"delete_permissions\":[],\"event_triggers\":[],\"computed_fields\":[]},{\"table\":{\"schema\":\"parkirin\",\"name\":\"zoneslots\"},\"is_enum\":false,\"configuration\":{\"custom_root_fields\":{\"select\":null,\"select_by_pk\":null,\"select_aggregate\":null,\"insert\":null,\"update\":null,\"delete\":null},\"custom_column_names\":{}},\"object_relationships\":[{\"using\":{\"foreign_key_constraint_on\":\"parkirinid\"},\"name\":\"parkirin\",\"comment\":null}],\"array_relationships\":[{\"using\":{\"manual_configuration\":{\"remote_table\":{\"schema\":\"parkirin\",\"name\":\"slots\"},\"column_mapping\":{\"parkirinid\":\"parkirinid\",\"id\":\"zoneslotid\"}}},\"name\":\"slots\",\"comment\":null}],\"insert_permissions\":[],\"select_permissions\":[],\"update_permissions\":[],\"delete_permissions\":[],\"event_triggers\":[],\"computed_fields\":[]}]}}";

        Document resultobj = executeHasuraQuery(query);
        if(debugenabled){
            System.out.println("executeHasuraQuery result doc = " + resultobj.toJson());
        }

        return resultobj;
    }

    private Document executeGraphQLQuery(String query){
        if(debugenabled){
            System.out.println("executeGraphQLQuery called with hasura url =  " + hasuraserviceurl + "/v1/graphql and query=" + query);
        }
        GraphQLQuery topologyQuery = new GraphQLQuery();
        // Use a singletonMap to retain the object name
        // topologyQuery.setVariables(Collections.singletonMap("duration", duration));
        topologyQuery.setQuery(query);
        
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> resultobj = restTemplate.postForEntity((hasuraserviceurl + "/v1/graphql"), topologyQuery, String.class);
        String responsebody = resultobj.getBody();
        if(debugenabled){
            System.out.println("executeGraphQLQuery results string response  = " + responsebody);
        }

        Document results =  Document.parse(responsebody);
        return results;
    }

    private Document executeHasuraQuery(String query){
        if(debugenabled){
            System.out.println("executeHasuraQuery called with hasura url =  " + hasuraserviceurl + "/v1/query and body=" + query);
        }

        Document requestbody = Document.parse(query);
        
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-Hasura-Role", "admin");

        HttpEntity<String> entity = new HttpEntity<>(requestbody.toJson(), headers);
        try{
            ResponseEntity<String> resultobj = restTemplate.postForEntity((hasuraserviceurl + "/v1/query"), entity, String.class);

            String responsebody = resultobj.getBody();
            if(debugenabled){
                System.out.println("executeHasuraQuery results string response  = " + responsebody);
            }

            Document results =  Document.parse(responsebody);
            return results;
        }
        catch(HttpClientErrorException e){
            if(debugenabled){
                System.out.println("Exception executeHasuraQuery = " + e.toString());
                System.out.println("Body executeHasuraQuery = " + e.getResponseBodyAsString());
            }
            return new Document();
        }
    }
}