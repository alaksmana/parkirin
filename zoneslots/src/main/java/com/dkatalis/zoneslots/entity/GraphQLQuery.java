package com.dkatalis.zoneslots.entity;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GraphQLQuery {
    @JsonProperty("variables")
    private Object variables;
    
    @JsonProperty("query")
    private String query;
    

    public GraphQLQuery() {
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}