package com.dkatalis.zoneslots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZoneslotsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZoneslotsApplication.class, args);
	}
}
