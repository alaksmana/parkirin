package com.dkatalis.zoneslots.payload;

public class UploadFileResponse<T> {
    private String service;
    private String message;
    private T data;

    public String getService() {
        return service;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setService(String service) {
        this.service = service;
    }
}