# Parkirin

Click for more details on coverage:

[![coverage](https://gitlab.com/alaksmana/parkirin/badges/master/coverage.svg?job=testversion)](https://alaksmana.gitlab.io/parkirin/) 

![pipeline](https://gitlab.com/alaksmana/parkirin/badges/master/pipeline.svg)

## Overview

Parkirin is an application which contains:
Parkirin (Parking site which can have many Zoneslots)
Zoneslots (Parking zone which can have many slots)
X number of Entrypoint
X number of Exitpoint
Filereader 
Each function is represented with API.

## Quick Install

1. Install Docker https://docs.docker.com/v17.09/engine/installation/
2. Install docker-compose (it comes with the docker installation)
3. Install Postman (Test REST API)
4. Run docker compose

```
$ cd docker-compose
$ docker-compose up -d
```

5. When all services are up, Go to hasura http://localhost:58080/console/data/schema/parkirin
6. You will see `Untracked tables or views [Track All] ` 
   Click `[Track All]`
   Hasura will ask `Are you sure? This will expose all the listed tables/views over the GraphQL API. `
   Click `OK`.
7. You will see `Untracked foreign-key relations [Track All]`
   Click `[Track All]`
   Hasura will ask `Are you sure? This will add all the listed foreign-keys as relationships in the GraphQL Schema.`
   Click `OK`
8. Open Postman
9. Import PARKIRIN.postman_collection.json into Postman
10. Run `POST` `/parkirin/fileinput` , for more details check the section on `REST API documentation`
   

## Zoneslots

A Zoneslots contain N number of parking slots.
A Zone has x number of Entrypoint client.
A Zone has x number of Exitpoint client.

It has a list of all the registered cars and each respective time.

To ensure atomicity, the database table is designed with PKID as the slot id.

`CREATE_PARKING_LOT` command means, create a Zoneslots with N number of slot.

`PARK` command means, an Entrypoint :

Ask ZoneSlots if a nearest slot is available?
If yes, add a row to the Zoneslots table with pkid of the slot no.
Then add historical log to the history table.

`LEAVE` command means, an Exitpoint :

Ask ZoneSlots what is the charges given the hour no.
Delete the row with pkid of the slot no.
Then add historical log to the history table.

`STATUS` command means, list all slots and car registered at a given Zoneslots.

## Filereader

File Reader will read an input text file.

Input text file contains commands and parameters in the above Overview explanation which will be translated into Zoneslots REST API / Graphql.

The target Zoneslots id should be a required parameter in this File Reader.

You can see `parking_lot_file_inputs.txt` which contains series of commands and parameters.

## Zoneslots Implementation

Although we use Hasura GraphQL as dynamic service, the business logic is implemented as REST API call which is mapped into the GraphQL as REST endpoint.

Of course in the future we can always convert this into GraphQL client (like Apollo, Prisma or Spring).

# REST API documentation:

## `POST` `/parkirin/createparkinglot` 
Create a new Zoneslots for a Parkirinid site. 

Request Post body sample:
```
{
	"name" : "Zone A Menara Prima",
	"totalslots" : 6
}
```

Sample Response result:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.createParkingLot",
    "message": "Method execution successful!",
    "data": {
        "result": {
            "data": {
                "insert_parkirin_zoneslots": {
                    "returning": [
                        {
                            "id": 4,
                            "name": "Zone A Menara Prima",
                            "totalslots": 6,
                            "parkirinid": 1
                        }
                    ]
                }
            }
        }
    }
}
```

Sample If missing property:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.createParkingLot",
    "message": "Method execution fail!",
    "data": {
        "error": "Missing name property."
    }
}
```

## `POST` `/parkirin/park` 
This will add an entry in Slots belonging to a Zone and a Site.

Request Post body sample:
```
{
	"zoneslotid" : 1,
	"slotid" : 4,
	"registrationnumber" : "DK1234GG",
	"color" : "GREEN" 
}
```

Sample Response result if successful:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.executePark",
    "message": "Method execution successful!",
    "data": {
        "result": {
            "data": {
                "insert_parkirin_slots": {
                    "returning": [
                        {
                            "parkirinid": 1,
                            "zoneslotid": 1,
                            "id": 2,
                            "registrationnumber": "DK1234GG",
                            "entrytime": "2019-12-09T09:45:19.794288+00:00",
                            "color": "unknown"
                        }
                    ]
                }
            }
        }
    }
}
```

Sample If missing property:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.executePark",
    "message": "Method execution fail!",
    "data": {
        "error": "Missing slotid property."
    }
}
```

## `POST` `/parkirin/leave` 
This will remove an entry in Slots belonging to a Zone and a Site.

Request Post body sample:
```
{
	"zoneslotid" : 1,
	"slotid" : 4,
	"registrationnumber" : "DK1234GG",
	"color" : "GREEN" 
}
```

Sample Response result if successful:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.executeLeave",
    "message": "Method execution successful!",
    "data": {
        "result": {
            "data": {
                "delete_parkirin_slots": {
                    "returning": [
                        {
                            "parkirinid": 1,
                            "zoneslotid": 1,
                            "id": 2,
                            "registrationnumber": "DK1234GG",
                            "color": "unknown",
                            "entrytime": "2019-12-09T09:45:19.794288+00:00"
                        }
                    ]
                }
            }
        }
    }
}
```

Sample If missing property:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsController.executeLeave",
    "message": "Method execution fail!",
    "data": {
        "error": "Missing registrationnumber property."
    }
}
```

##  `GET` `/parkirin/status` 
This will return all zoneslots their slot contained.
In the future, we need to add pagination and filtering here.

Example response:
```
{
    "service": "com.dkatalis.zoneslots.controller.ZoneslotsControllerexecuteStatus",
    "message": "Method execution successful!",
    "data": {
        "result": {
            "data": {
                "parkirin_zoneslots": [
                    {
                        "id": 1,
                        "name": "Zona 1 Menara Prima",
                        "parkirinid": 1,
                        "totalslots": 6,
                        "slots": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 1,
                                "registrationnumber": "B123K",
                                "entrytime": "2019-12-09T05:08:46.293867+00:00",
                                "color": "unknown"
                            },
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 6,
                                "registrationnumber": "B1612UJI",
                                "entrytime": "2019-12-09T08:55:02.870942+00:00",
                                "color": "GREY"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "Zone 2 Menara Prima",
                        "parkirinid": 1,
                        "totalslots": 6,
                        "slots": []
                    }
                ]
            }
        }
    }
}
```

## `POST` `/parkirin/fileinput` 

Sample Request, please change the path of the `parking_lot_file_inputs.txt` accordingly:
```
curl -X POST \
  http://localhost:8080/parkirin/fileinput \
  -F file=@/Users/ande/Documents/GITLAB/ALAKSMANA/parkirin/parking_lot_file_inputs.txt
```
This will execute the File for all commands and parameters.

Sample Result:

```
{
    "service": "com.dkatalis.zoneslots.controller.ImportControllerexecuteFile",
    "message": "File execution successful!",
    "data": {
        "output1": {
            "result": {
                "data": {
                    "insert_parkirin_zoneslots": {
                        "returning": [
                            {
                                "id": 17,
                                "name": "6",
                                "totalslots": 6,
                                "parkirinid": 1
                            }
                        ]
                    }
                }
            }
        },
        "output2": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 1,
                                "registrationnumber": "KA-01-HH-1234",
                                "entrytime": "2019-12-09T17:11:08.443415+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output3": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 2,
                                "registrationnumber": "KA-01-HH-9999",
                                "entrytime": "2019-12-09T17:11:08.507417+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output4": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 3,
                                "registrationnumber": "KA-01-BB-0001",
                                "entrytime": "2019-12-09T17:11:08.555328+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output5": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 4,
                                "registrationnumber": "KA-01-HH-7777",
                                "entrytime": "2019-12-09T17:11:08.585612+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output6": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 5,
                                "registrationnumber": "KA-01-HH-2701",
                                "entrytime": "2019-12-09T17:11:08.610516+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output7": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 6,
                                "registrationnumber": "KA-01-HH-3141",
                                "entrytime": "2019-12-09T17:11:08.634644+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output8": {
            "result": {
                "data": {
                    "delete_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 6,
                                "registrationnumber": "KA-01-HH-3141",
                                "color": "unknown",
                                "entrytime": "2019-12-09T17:11:08.634644+00:00"
                            }
                        ]
                    }
                }
            }
        },
        "output9": {
            "result": {
                "data": {
                    "parkirin_zoneslots": [
                        {
                            "id": 1,
                            "name": "zone 1 menara prima",
                            "parkirinid": 1,
                            "totalslots": 6,
                            "slots": [
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 1,
                                    "registrationnumber": "KA-01-HH-1234",
                                    "entrytime": "2019-12-09T17:11:08.443415+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 2,
                                    "registrationnumber": "KA-01-HH-9999",
                                    "entrytime": "2019-12-09T17:11:08.507417+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 3,
                                    "registrationnumber": "KA-01-BB-0001",
                                    "entrytime": "2019-12-09T17:11:08.555328+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 4,
                                    "registrationnumber": "KA-01-HH-7777",
                                    "entrytime": "2019-12-09T17:11:08.585612+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 5,
                                    "registrationnumber": "KA-01-HH-2701",
                                    "entrytime": "2019-12-09T17:11:08.610516+00:00",
                                    "color": "unknown"
                                }
                            ]
                        },
                        {
                            "id": 17,
                            "name": "6",
                            "parkirinid": 1,
                            "totalslots": 6,
                            "slots": []
                        }
                    ]
                }
            }
        },
        "output10": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 6,
                                "registrationnumber": "KA-01-P-333",
                                "entrytime": "2019-12-09T17:11:08.683347+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output11": {
            "error": "Zoneslots is full! No parking slot available!"
        },
        "output12": {
            "result": {
                "data": {
                    "delete_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 1,
                                "registrationnumber": "KA-01-HH-1234",
                                "color": "unknown",
                                "entrytime": "2019-12-09T17:11:08.443415+00:00"
                            }
                        ]
                    }
                }
            }
        },
        "output13": {
            "result": {
                "data": {
                    "delete_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 3,
                                "registrationnumber": "KA-01-BB-0001",
                                "color": "unknown",
                                "entrytime": "2019-12-09T17:11:08.555328+00:00"
                            }
                        ]
                    }
                }
            }
        },
        "output14": {
            "result": {
                "data": {
                    "delete_parkirin_slots": {
                        "returning": []
                    }
                }
            }
        },
        "output15": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 1,
                                "registrationnumber": "KA-09-HH-0987",
                                "entrytime": "2019-12-09T17:11:08.783813+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output16": {
            "result": {
                "data": {
                    "insert_parkirin_slots": {
                        "returning": [
                            {
                                "parkirinid": 1,
                                "zoneslotid": 1,
                                "id": 3,
                                "registrationnumber": "CA-09-IO-1111",
                                "entrytime": "2019-12-09T17:11:08.823832+00:00",
                                "color": "unknown"
                            }
                        ]
                    }
                }
            }
        },
        "output17": {
            "error": "Zoneslots is full! No parking slot available!"
        },
        "output18": {
            "result": {
                "data": {
                    "parkirin_zoneslots": [
                        {
                            "id": 1,
                            "name": "zone 1 menara prima",
                            "parkirinid": 1,
                            "totalslots": 6,
                            "slots": [
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 2,
                                    "registrationnumber": "KA-01-HH-9999",
                                    "entrytime": "2019-12-09T17:11:08.507417+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 4,
                                    "registrationnumber": "KA-01-HH-7777",
                                    "entrytime": "2019-12-09T17:11:08.585612+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 5,
                                    "registrationnumber": "KA-01-HH-2701",
                                    "entrytime": "2019-12-09T17:11:08.610516+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 6,
                                    "registrationnumber": "KA-01-P-333",
                                    "entrytime": "2019-12-09T17:11:08.683347+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 1,
                                    "registrationnumber": "KA-09-HH-0987",
                                    "entrytime": "2019-12-09T17:11:08.783813+00:00",
                                    "color": "unknown"
                                },
                                {
                                    "parkirinid": 1,
                                    "zoneslotid": 1,
                                    "id": 3,
                                    "registrationnumber": "CA-09-IO-1111",
                                    "entrytime": "2019-12-09T17:11:08.823832+00:00",
                                    "color": "unknown"
                                }
                            ]
                        },
                        {
                            "id": 17,
                            "name": "6",
                            "parkirinid": 1,
                            "totalslots": 6,
                            "slots": []
                        }
                    ]
                }
            }
        }
    }
}
```

# RAPID FRAMEWORK

# Overview
Rapid Development Framework
Flutter-Kong|plugins|Keycloak|TIG|ELK-ActivitiBPM-Hasura|Springboot-PostgresCluster

Microservices Architecture, Cloud native:  
Front End = Flutter
Gateway = Kong with custom plugins and Konga GUI
IDM = Keycloak
Logging & Visualization = Elasticsearch Logstash Kibana
Monitoring and alert = Dockprom stack OR alternative Telegraf InfluxDB Grafana
BPM engine = Activiti Cloud
BaaS = Hasura Graphql
Any other custom service = Springboot Java
Database = PostgresCluster

# Kong Custom Plugin
https://github.com/gbbirkisson/kong-plugin-jwt-keycloak
https://github.com/alaksmana/kong-oidc-ws-rbac

Docker:  alaksmana/kong-oidc:jwt

#  Sample working configuration for Kong Plugin
10.0.1.10 must be replaced with the server ip or domain name.

```
{
  "createdUser": null,
  "updatedUser": null,
  "id": 1,
  "name": "rapid_v1",
  "kong_node_name": "rapid",
  "kong_node_url": "http://kong:8001",
  "kong_version": "1.3.0",
  "data": {
    "services": [
      {
        "host": "graphql-engine",
        "created_at": 1575634279,
        "connect_timeout": 60000,
        "id": "b5ed2c3e-904d-4e04-a842-616f660c7e53",
        "protocol": "http",
        "name": "hasura",
        "read_timeout": 60000,
        "port": 8080,
        "path": "/v1/graphql",
        "updated_at": 1575634279,
        "retries": 5,
        "write_timeout": 60000,
        "tags": [],
        "client_certificate": null,
        "extras": {
          "id": 1,
          "service_id": "b5ed2c3e-904d-4e04-a842-616f660c7e53",
          "kong_node_id": "5",
          "description": "Hasura Graphql",
          "tags": null,
          "createdAt": "2019-12-06T12:11:19.000Z",
          "updatedAt": "2019-12-06T12:11:19.000Z",
          "createdUser": null,
          "updatedUser": null
        }
      }
    ],
    "routes": [
      {
        "id": "691c4a9a-fb24-45a7-9010-c7d3691331b5",
        "tags": null,
        "updated_at": 1575634973,
        "destinations": null,
        "headers": null,
        "protocols": [
          "http",
          "https"
        ],
        "created_at": 1575634356,
        "snis": null,
        "service": {
          "id": "b5ed2c3e-904d-4e04-a842-616f660c7e53"
        },
        "name": "hasura",
        "preserve_host": false,
        "regex_priority": 0,
        "strip_path": true,
        "sources": null,
        "paths": [
          "/graphql"
        ],
        "https_redirect_status_code": 426,
        "hosts": [],
        "methods": []
      }
    ],
    "consumers": [],
    "plugins": [
      {
        "created_at": 1575657640,
        "config": {
          "client_roles": null,
          "allowed_iss": [
            "http://10.0.1.10:58180/auth/realms/rpm"
          ],
          "run_on_preflight": true,
          "iss_key_grace_period": 10,
          "maximum_expiration": 0,
          "claims_to_verify": [],
          "consumer_match_claim_custom_id": false,
          "cookie_names": [],
          "scope": null,
          "uri_param_names": [],
          "roles": null,
          "consumer_match": false,
          "well_known_template": "http://10.0.1.10:58180/auth/realms/rpm/.well-known/openid-configuration",
          "consumer_match_ignore_not_found": false,
          "anonymous": null,
          "algorithm": "RS256",
          "realm_roles": [
            "user",
            "admin"
          ],
          "consumer_match_claim": "azp"
        },
        "id": "59d31338-02d8-4e6d-b447-8152b72b4293",
        "service": null,
        "name": "jwt-keycloak",
        "protocols": [
          "grpc",
          "grpcs",
          "http",
          "https"
        ],
        "enabled": true,
        "run_on": "first",
        "consumer": null,
        "route": null,
        "tags": null
      },
      {
        "created_at": 1575655126,
        "config": {
          "response_type": "code",
          "introspection_endpoint": "http://10.0.1.10:58180/auth/realms/rpm/protocol/openid-connect/token/introspect",
          "timeout": null,
          "filters": null,
          "bearer_only": "yes",
          "ssl_verify": "no",
          "session_secret": null,
          "introspection_endpoint_auth_method": "client_secret_post",
          "realm": "rpm",
          "redirect_after_logout_uri": "/",
          "scope": "openid",
          "token_endpoint_auth_method": "client_secret_post",
          "client_secret": "c2a3148f-046a-4017-8f8e-54a24d6a8652",
          "client_id": "rpm",
          "logout_path": "/logout",
          "discovery": "http://10.0.1.10:58180/auth/realms/master/.well-known/openid-configuration",
          "recovery_page_path": null,
          "redirect_uri_path": null
        },
        "id": "8a003981-3e14-4acd-8e85-7179c0557bce",
        "service": null,
        "name": "oidc",
        "protocols": [
          "grpc",
          "grpcs",
          "http",
          "https"
        ],
        "enabled": true,
        "run_on": "first",
        "consumer": null,
        "route": null,
        "tags": null
      }
    ],
    "acls": [],
    "upstreams": [],
    "certificates": [],
    "snis": []
  },
  "createdAt": "2019-12-06T18:43:08.000Z",
  "updatedAt": "2019-12-06T18:43:08.000Z"
}
```

# Kong scenarios

## If User does not have a valid token

```
401 Unauthorized

invalid token
```

## If  User does not have any role assigned OR If User has a role but unauthorized

```
403 Forbidden

{
    "message": "Access token does not have the required scope/role: Missing required realm role"
}
```

# How to query security matrix from Kong schema

```
SELECT A.id AS service_id, A.protocol, A.host, A.port, A.path, 
B.id AS route_id, B.paths, B.service_id, B.methods,
C.route_id, C.config->>'realm_roles'
FROM kong.services A
LEFT JOIN kong.routes AS B ON A.id = B.service_id
LEFT JOIN kong.plugins AS C ON B.id = C.route_id
WHERE C.name = 'jwt-keycloak';
```

The above method will list all paths and its available methods and roles.
Sample result:

```
"service_id","protocol","host","port","path","route_id","paths","service_id-2","methods","route_id-2","?column?"
"b5ed2c3e-904d-4e04-a842-616f660c7e53","http","graphql-engine","8080","/v1/graphql","691c4a9a-fb24-45a7-9010-c7d3691331b5","{/graphql}","b5ed2c3e-904d-4e04-a842-616f660c7e53","{POST,GET,PUT,DELETE,APPROVAL}","691c4a9a-fb24-45a7-9010-c7d3691331b5","[""admin"", ""user""]"
```

Notice we can actually use custom method like `APPROVAL` and therefore will call the appropriate BPM process.

# Kong - Elasticsearch | Logstash | Kibana

Kong needs to be configured with UDP Plugin and stream to port 5000:

```
$ http --form POST :8001/services/chuck/plugins \
  name=udp-log \
  config.host=logstash \
  config.port=5000
```
This demonstration setup:

a indexed document database (Elasticksearch)
a log collector and transformer (Logstash)
a data visualization & monitoring platform (Kibana)
a Kong instance with a minimal configuration: an API with the UDP-log plugin.
The purpose of this demo is to produce logs from the API gateway and build some visualization dashboard.

Now any access of the API will produce JSON logs gathered by Logstash and stored into Elasticsearch.

Let's call the API several times:

$ watch -n 2 http :8000/chuck
Open your browser and go to the Kibana console (http://localhost:5601):

Note that Kibana is very long to start the first time.

Configure an index pattern: logstash-*
You should see many fields.
Click on Discover and you should see incoming events.
Import new dashboard:
Management->Saved Objects->Import
Choose ../dashboards/kibana.json

You should see visualization of HTTP response metrics in the example.

# Dockprom Stack OR Kong - Telegraf | InfluxDB | Grafana
Alternative stack to ELK stack.

# Flutter Web
CRUD GUI
Dockerize
Add Docker compose
CI auto version and upload dockerhub

# Katalon Test
Login
CRUD
CI

# Deployment to Kubernetes
Prepare Infra
Create K8s script
Create Helm
CD script gitlab